﻿using System;
using System.Threading;

namespace BtTest
{
    class Program
    {
        static void Main(string[] args)
        {
            BtTestLib.BtTester tester = new BtTestLib.BtTester();
            tester.getDevice("One");
            Thread.Sleep(2000);
            String[] list = tester.getDeviceList();
            foreach (String s in list)
            {
                Console.WriteLine("Device found: " + s);
            }

            Thread.Sleep(2000);
            Console.WriteLine("Data: " + tester.getData());
            Console.WriteLine("Callback called: " + tester.getWorked());
        }
    }
}
